package SuperMercado.View;

import SuperMercado.Model.Estoque;
import SuperMercado.Model.Pedido;
import SuperMercado.Model.Produto;
import Utils.Inputs;


import java.util.InputMismatchException;

import static SuperMercado.Model.Estoque.*;
import static SuperMercado.Model.Pedido.*;
import static SuperMercado.Model.CalculoTroco.*;
import static Utils.Inputs.*;

public class Menu {

    //private static final Pedido pedido = new Pedido(); TODO excluir
    //private static final Estoque estoque = new Estoque(); // - NÃO PRECISA CRIAR O OBJETO SE NÃO VAI TER MAIS DE UMA INSTÂNCIA

    public static void controleMenu() {
        Estoque.inicializaEstoque();
        do {
            mostraMenu();
        } while (switchCase());
    }

    public static void mostraMenu() {
        System.out.println("------------MENU------------");
        System.out.println("1 - MOSTRAR CATALOGO");
        System.out.println("2 - ADICIONAR ITEM AO CARRINHO");
        System.out.println("3 - VER PEDIDO");
        System.out.println("4 - ALTERAR O VALOR DE UM PRODUTO");
        System.out.println("5 - ALTERAR A QUANTIDADE DE UM PRODUTO");
        System.out.println("6 - MOSTRAR SOMENTE PRODUTOS COM ESTOQUE ZERADO");
        System.out.println("7 - PAGAMENTO EM DINHEIRO");
        System.out.println("\n0 - ENCERRAR\n");
    }

    public static boolean switchCase() {
        Produto produto;

        System.out.println("ESCOLHA UMA OPÇÃO: ");

        try { //TODO apg 07-ago-2020 - exception
            switch (Inputs.inputInt()) {
                case 1:
                    Estoque.imprimeCatalagoDoEstoque();
                    return true;
                case 2: {
                    System.out.println("2 - ADICIONAR ITEM AO CARRINHO\n");
                    produto = recebeNomeDoProdutoDoTeclado();
                    int quantidade = recebeQuantidadeDoTeclado(produto);
                    adicionaItemNaLista(produto, quantidade);
                    calculaValorTotal();
                    return true;
                }
                case 3: {
                    System.out.println("3 - VER PEDIDO\n");
                    if (!verificaSeListaDeItensTemItens()) {
                        System.out.println("Pedido está vazio!\n\n");
                    } else {
                        imprimePedidoComTotal();
                    }
                    return true;
                }
                //TODO - apg
                case 4: {
                    System.out.println("4 - ALTERAR O VALOR DE UM PRODUTO\n");
                    produto = recebeNomeDoProdutoDoTeclado();
                    alteraValorDeProdutoCadastradoNoEstoque(produto);
                    return true;
                }
                case 5: {
                    System.out.println("5 - ALTERAR A QUANTIDADE DE UM PRODUTO\n");
                    produto = recebeNomeDoProdutoDoTeclado();
                    alteraQuantidadeDeProdutoNoEstoque(produto);
                    return true;
                }
                case 6: {
                    System.out.println("6 - MOSTRAR SOMENTE PRODUTOS COM ESTOQUE ZERADO\n");
                    mostrarSomenteProdutosComEstoqueZerado();
                    return true;
                }
                //TODO * (se der tempo1) + 1 método, receba o valor que vai ser pago, calcular o troco, e imprimir na tela a menor qtd de notas (notas e moedas = notas) possíveis para o troco. (vide caixa eletrônico)
                case 7: {
                    System.out.println("7 - PAGAMENTO EM DINHEIRO");
                    pagamentoEmDinheiro();
                    return true;
                }
                case 0:
                    return false;
                default:
                    System.out.println("Opção inválida");
                    return true;
            }

        } catch (InputMismatchException exception) {
            System.out.println("        POR FAVOR DIGITE APENAS NÚMEROS INTEIROS!\n\n");
        }
        return true;
    }

    public static Produto recebeNomeDoProdutoDoTeclado(){
        System.out.print("Digite o nome do produto: ");
        String nomeProduto = Inputs.inputString();
        Produto produto = encontraProduto(nomeProduto);
        boolean flagProdutoNotNull = false;
        while(!flagProdutoNotNull) {
            if (produto != null) {
                flagProdutoNotNull = true;
                return produto;
            } else {
                System.out.println("Produto não encontrado!");
                nomeProduto = null;
                System.out.print("Digite o nome do produto: ");
                nomeProduto = Inputs.inputString();
                produto = encontraProduto(nomeProduto);
            }
        }

        return produto;
    }

    public static int recebeQuantidadeDoTeclado(Produto produto){
        //Produto produto = recebeNomeDoProdutoDoTeclado();
        int quantidade = 0;
        //TODO apg 08-ago-2020
        System.out.print("Digite a quantidade ou '0' para sair: ");
        quantidade = Inputs.inputInt();

        if (verificaSeHaEstoque(produto, quantidade)) return quantidade;
        else System.out.println("Não há quantidade no Estoque.");

        return quantidade;
    }

    public static void pagamentoEmDinheiro() {
        double dinheiroParaPagamento;

        if (verificaSeListaDeItensTemItens()) {
            imprimeValorTotal();
            System.out.print("Valor recebido: R$ ");
            dinheiroParaPagamento = inputDouble();
            verificaSeDinheiroRecebidoSuficiente(dinheiroParaPagamento);
        }

        //System.out.println("Lista de itens está vazia!\n");
    }

}
