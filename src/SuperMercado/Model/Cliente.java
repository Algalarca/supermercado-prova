package SuperMercado.Model;

public class Cliente {

    private int indice;
    private String nome;

    public Cliente(int indice, String nome) {
        this.indice = indice;
        this.nome = nome;
    }

    public int getInd() {
        return indice;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }
}
