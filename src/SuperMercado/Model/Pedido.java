package SuperMercado.Model;

import java.util.ArrayList;

import static SuperMercado.Model.Estoque.getQuantidadeAtualEmEstoque;
import static SuperMercado.Model.Estoque.verificaSeHaEstoque;

public class Pedido {

    private static ArrayList<Item> listaDeItens = new ArrayList();
    private static double valorTotalDoPedido = 0;

    public static void calculaValorTotal() {
        double subTotal = 0;
        for (Item item : getListaDeItens()) {
            subTotal += item.getValorDoItem();
        }
        valorTotalDoPedido = subTotal;
    }

    //TODO arrumar a baixa do estoque para quando nao tiver estoque o suficiente
    //TODO fixed
    public static boolean adicionaItemNaLista(Produto produto, int quantidade) {
        if(quantidade > 0) {
            if (verificaSeHaEstoque(produto, quantidade)) {
                for (Item item : listaDeItens) {
                    if (item.getProduto().getNome().equalsIgnoreCase(produto.getNome())) {
                        //Estoque.darBaixaEmEstoque(item.getProduto().getId(), quantidade);
                        //if(Estoque.darBaixaEmEstoque(item.getProduto().getId(), quantidade)){ TODO arrumar - apg
                        Estoque.darBaixaEmEstoque(item.getProduto().getId(), quantidade);
                        item.setQuantidade(item.getQuantidade() + quantidade);
                        item.defineValorTotal();
                        System.out.println("\nFoi adicionada a quantidade ao item já existente.\n\n");
                        return false;
                        //} TODO arrumar - apg
                    }
                }
                listaDeItens.add(new Item(produto, quantidade));
                Estoque.darBaixaEmEstoque(produto.getId(), quantidade);
                //TODO fixed - apg
                System.out.println("\nFoi adicionado " + produto.getNome() + " na lista de compras.\n\n");
                return true;
            } else if (quantidade <= 0)
                System.out.println("Adição de produto no carrinho descartada!");
                return false;
        }
            return false;
    }

    public static void imprimePedidoComTotal() {
        System.out.println("                              NOTA FISCAL");
        System.out.printf("ID       |NOME            |PRECO UN           |QUANTIDADE   |PRECO ITEM \n");
        for (Item item : listaDeItens) {
            System.out.printf("%-8d | %-14s | R$%-15.2f | %-10d  | R$%.2f\n"
                    , item.getProduto().getId(), item.getProduto().getNome(), item.getProduto().getPreco(), item.getQuantidade(), item.getValorDoItem());
        }
        imprimeValorTotal();
    }

    public static void imprimeValorTotal() {
        System.out.println();
        System.out.printf("Total: R$%.2f", valorTotalDoPedido);
        System.out.println("\n________________________________________________________________________\n");
    }

    public static boolean verificaSeListaDeItensTemItens(){
        if(!listaDeItens.isEmpty()) return true;

        return false;
    }

    public static void limparCarrinho() {
        listaDeItens.clear();
    }

    public static ArrayList<Item> getListaDeItens() {
        return listaDeItens;
    }

    public static void setListaDeItens(ArrayList<Item> listaDeItens) {
        listaDeItens = listaDeItens;
    }

    public static double getValorTotalDoPedido() {
        return valorTotalDoPedido;
    }

    public static void setValorTotalDoPedido(double valorTotalDoPedido) {
        valorTotalDoPedido = valorTotalDoPedido;
    }
}
