package SuperMercado.Model;

import static SuperMercado.View.Menu.*;
import Utils.CsvToArray;
import Utils.Inputs;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import java.io.IOException;
import java.util.ArrayList;

public class Estoque {

    private static int id = 1;
    private final static ArrayList<Produto> listaDeProdutos = new ArrayList();

    public static void inicializaEstoque() {
        try {
            ArrayList<String[]> listaAuxiliar = CsvToArray.csvToArrayList("ListaDeProdutos.CSV");
            for (String[] produto : listaAuxiliar.subList(1, listaAuxiliar.size())) {
                String nome = produto[0];
                double preco = Double.parseDouble(produto[1]);
                int quantidade = Integer.parseInt(produto[2]);
                cadastraProduto(new Produto(id++, nome, preco, quantidade));
            }
        } catch (IOException e) {
            System.out.println("Arquivo nao encontrado, o catalogo inicializara vazio");
        }
    }

    public static boolean cadastraProduto(@NotNull Produto produto) {
        if (encontraProduto(produto.getNome()) == null) {
            listaDeProdutos.add(produto);
            return true;
        } else {
            return false;
        }
    }

    public static @Nullable Produto encontraProduto(String nome) {
        for (Produto produto : listaDeProdutos) {
            if (produto.getNome().equalsIgnoreCase(nome))  {
                return produto;
            }
        }
        return null;
    }

    public static @Nullable Produto encontraProduto(int id) {
        for (Produto produto : listaDeProdutos) {
            if (produto.getId() == id){
                return produto;
            }
        }
        return null;
    }

    public static void imprimeCatalagoDoEstoque() {
        System.out.println("                              CATALOGO");
        System.out.println("________________________________________________________________________");
        System.out.println("ID       |NOME                       |PRECO              |QUANTIDADE \n");
        for (Produto produto : listaDeProdutos) {
            if(produto.getQuantidadeEmEstoque() != 0) {
                System.out.printf("%-8d | %-25s | R$%-15.2f | %d\n"
                        , produto.getId(), produto.getNome(), produto.getPreco(), produto.getQuantidadeEmEstoque());
            }
        }
        System.out.println("\n________________________________________________________________________\n\n");
    }

    public static boolean darBaixaEmEstoque(String nome, int quantidadeParaDarBaixa) {
        Produto produto = encontraProduto(nome);
        if (produto != null) {
            if (verificaSeHaEstoque(produto, quantidadeParaDarBaixa)) {
                listaDeProdutos.get(getPosicaoDoProdutoNaLista(produto)).setQuantidadeEmEstoque(getQuantidadeAtualEmEstoque(produto) - quantidadeParaDarBaixa);
                return true;
            } else {
                System.out.println("Não tem quantidade o suficiente de " + produto.getNome());
                return false;
            }
        } else {
            System.out.println("Produto não encontrado para dar baixa no estoque");
            return false;
        }
    }

    public static boolean darBaixaEmEstoque(int id, int quantidadeParaDarBaixa) {
//        TODO tornar boolean e adicionar retorno - OK - apg
//        TODO fixed
        Produto produto = encontraProduto(id);
        if ((!produto.equals(null)) && (verificaSeHaEstoque(produto, quantidadeParaDarBaixa))) {
                listaDeProdutos.get(getPosicaoDoProdutoNaLista(produto)).setQuantidadeEmEstoque(getQuantidadeAtualEmEstoque(produto) - quantidadeParaDarBaixa);
                return true;
        } else {
            System.out.println("Produto não encontrado para dar baixa no estoque");
            return false;
        }
    }

    //TODO 31-07-2020 - apg
    public static void alteraValorDeProdutoCadastradoNoEstoque(Produto produto){
        imprimeProduto(produto);
        System.out.println("");
        System.out.print("Digite o novo valor: R$ ");
        double novoValor = Inputs.inputDouble();
        System.out.println("");
        System.out.println("Valor antigo: R$ " + produto.getPreco());
        System.out.println("Novo valor: R$ " + novoValor);
        confirmaAlteracao(produto, novoValor);
    }

    //TODO 31-07-2020 - apg
    //TODO tratar exceção
    public static void alteraQuantidadeDeProdutoNoEstoque(Produto produto){
        imprimeProduto(produto);
        System.out.println("");
        System.out.print("Digite a nova quantidade: ");
        int novaQuantidade = Inputs.inputInt();
        System.out.println("");
        System.out.println("Quantidade antiga: " + produto.getQuantidadeEmEstoque());
        System.out.println("Nova quantidade: " + novaQuantidade);
        confirmaAlteracao(produto, novaQuantidade);
    }

    //TODO 31-07-2020 - apg
    public static void confirmaAlteracao(Produto produto, double novoValor){
        System.out.println("Confirma alteração? (1 -> S / 2 -> N) "); // TODO tratar exceção

        switch (Inputs.inputInt()) {
            case 1: {
                produto.setPreco(novoValor);
                System.out.println("Alterado com sucesso!\n");
                imprimeProduto(produto);
                System.out.println("\n\n");
                break;
            }
            case 2:
                System.out.println("Alteração descartada!");
                System.out.println("\n\n");
                break;
            default:
                System.out.println("Opção inválida");
                break;
        }
    }

    //TODO 31-07-2020 - apg
    public static void confirmaAlteracao(Produto produto, int novaQuantidade){
        System.out.println("Confirma alteração? (1 -> S / 2 -> N) "); // TODO tratar exceção

        switch (Inputs.inputInt()) {
            case 1: {
                produto.setQuantidadeEmEstoque(novaQuantidade);
                System.out.println("Alterado com sucesso!\n");
                imprimeProduto(produto);
                System.out.println("\n\n");
                break;
            }
            case 2:
                System.out.println("Alteração descartada!");
                System.out.println("\n\n");
                break;
            default:
                System.out.println("Opção inválida");
                break;
        }
    }

    //TODO 31-07-2020 - apg
    public static void mostrarSomenteProdutosComEstoqueZerado(){
        System.out.println("                              CATALOGO");
        System.out.println("________________________________________________________________________");
        System.out.printf("ID       |NOME                       |PRECO              |QUANTIDADE \n");
        for (Produto produto : listaDeProdutos) {
            if(produto.getQuantidadeEmEstoque() == 0) {
                System.out.printf("%-8d | %-25s | R$%-15.2f | %d\n"
                        , produto.getId(), produto.getNome(), produto.getPreco(), produto.getQuantidadeEmEstoque());
            }
        }
        System.out.println("\n________________________________________________________________________\n\n");
    }

    //TODO 31-07-2020 - apg
    //TODO tratar exceção
    public static void imprimeProduto(Produto produto) {
        System.out.printf("ID       |NOME            |PRECO UN           |QUANTIDADE   |PRECO ITEM \n");
        try {
            System.out.printf("%-8d | %-14s | R$%-15.2f | %-10d  | R$%.2f\n"
                    , produto.getId(), produto.getNome(), produto.getPreco(), produto.getQuantidadeEmEstoque(), produto.getPreco());
            System.out.println("\n________________________________________________________________________\n");
            //Pedido.imprimeValorTotal(); TODO retirar
        }catch(NullPointerException exception){
            System.out.println("Produto não encontrado!\n\n");
        }

    }

    public static int getQuantidadeAtualEmEstoque(Produto produto) {
        return listaDeProdutos.get(getPosicaoDoProdutoNaLista(produto)).getQuantidadeEmEstoque();
    }

    public static int getPosicaoDoProdutoNaLista(Produto produto) {
        return getListaDeProdutos().indexOf(produto);
    }

    public static boolean verificaSeHaEstoque(Produto produto, int quantidadeParaDarBaixa) {
        int posicaoDoProdutoNaLista, quantidadeAtualEmEstoque;
        posicaoDoProdutoNaLista = getListaDeProdutos().indexOf(produto);
        quantidadeAtualEmEstoque = getListaDeProdutos().get(posicaoDoProdutoNaLista).getQuantidadeEmEstoque();
        if(quantidadeAtualEmEstoque >= quantidadeParaDarBaixa) {
            return true;
        } else {
            //TODO fixed
            System.out.println("Não tem quantidade o suficiente de " + produto.getNome());
            return false;
        }
    }

    public static ArrayList<Produto> getListaDeProdutos() {
        return listaDeProdutos;
    }
}
