package SuperMercado.Model;

import static SuperMercado.Model.Pedido.*;
import static SuperMercado.View.Menu.pagamentoEmDinheiro;

public class CalculoTroco {

    static double totalDoPedido = getValorTotalDoPedido();
    static double dinheiroRecebido = 0;

    public static void verificaSeDinheiroRecebidoSuficiente(double dinheiroParaPagamento) {
        dinheiroRecebido = dinheiroParaPagamento;
        int opcaoDaMensagemDeTroco;

        if (dinheiroRecebido == totalDoPedido) {
            opcaoDaMensagemDeTroco = 1;
            imprimeInformacoesDoTroco(opcaoDaMensagemDeTroco, dinheiroRecebido);
        }
        else if (dinheiroRecebido < totalDoPedido){
            opcaoDaMensagemDeTroco = 2;
            imprimeInformacoesDoTroco(opcaoDaMensagemDeTroco, dinheiroRecebido);
        }
        else{
            opcaoDaMensagemDeTroco = 3;
            imprimeInformacoesDoTroco(opcaoDaMensagemDeTroco, dinheiroRecebido);
        }

    }

    public static void imprimeInformacoesDoTroco(int mensagemDoTroco, double dinheiroParaPagamento){
        switch(mensagemDoTroco){
            case 1:
                System.out.println("Não há troco a ser devolvido!");
                limparCarrinho();
                break;
            case 2:
                System.out.println("Valor recebido é menor que o valor do pedido. Por favor, informe novamente.");
                pagamentoEmDinheiro();
                break;
            case 3:
                System.out.println(totalDoPedido);
                System.out.println();
                System.out.printf("Seu troco é: R$%.2f", calculaTrocoComMenorQuantidadeDeNotas(dinheiroParaPagamento));
                System.out.println();
                break;
        }
    }

    public static double calculaTrocoComMenorQuantidadeDeNotas(double dinheiroParaPagamento){
        double troco;

        troco = dinheiroParaPagamento - totalDoPedido;

        return troco;
    }

    public static void separaNotas(double notasParaSeremSeparadas){

    }


}
